<head>
    <title>EMC Alam Sutera</title>
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            var dataTable = $('#ext-grid').DataTable({
                "order": [
                    [2, "asc"]
                ],
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: "extas-grid-data.php", // json datasource
                    type: "post", // method  , by default get
                    error: function() { // error handling
                        $(".ext-grid-error").html("");
                        $("#ext-grid").append(
                            '<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>'
                            );
                        $("#ext-grid_processing").css("display", "none");

                    }
                }
            });

            $('.combo').on('change', function() { // for select box
                var i = $(this).attr('data-column');
                var v = $(this).val();
                dataTable.columns(i).search(v).draw();
            });
            $('.search-input-text').on('keyup click', function() { // for text boxes
                var i = $(this).attr('data-column'); // getting column index
                var v = $(this).val(); // getting search input value
                dataTable.columns(i).search(v).draw();
            });
            $('.search-input-ext').on('keyup click', function() { // for text boxes
                var i = $(this).attr('data-column'); // getting column index
                var v = $(this).val(); // getting search input value
                dataTable.columns(i).search(v).draw();
            });
            $("#ext-grid_filter").css("display", "none"); //hiding global search
            //$('.search-input-ext').focus() //set focus


        });
    </script>

    <link href="css/css.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="images/icon.png">

    <style>
        .display {
            border-radius: 10px;
            border: 2px solid rgba(157, 0, 0, 1);
        }

        div.container {
            margin: 0 auto;
            max-width: 760px;
        }

        div.header {
            margin: 100px auto;
            line-height: 30px;
            max-width: 760px;
        }

        body {
            color: #333;
            font-family: Ubuntu;
            font-size: 78%;
            line-height: 1.3em;
            background-color: #FFFFFF;
        }
    </style>
</head>


<body>
    <div class="logo-site"><img src="images/logo-omni.gif" width="190" height="56">
        <div class="tulisan">List Extension Alam Sutera</div>
    </div>
    <fieldset>

        <a href="index">
            <div class="kembali_menu">Back
            </div>
        </a>
        <div class="container">
            <div class="sites">

                <a href="corporate" title="OMNI CORPORATE"><span class="site-kecil">Corporate</span></a>
                <a href="alamsutera" title="OMNI ALAM SUTERA"><span class="site-aktif">AlamSutera</span></a>
                <a href="pulomas.html" title="OMNI PULOMAS"><span class="site-kecil">Pulomas</span></a>
                <a href="cikarang" title="OMNI CIKARANG"><span class="site-kecil">Cikarang</span></a>
                <a href="pekayon" title="OMNI PEKAYON"><span class="site-kecil">pekayon</span></a>
                <a href="kedoya" title="OMNI Graha Kedoya"><span class="site-kecil">Graha Kedoya</span></a>        
                <a href="cibitung" title="OMNI Cibitung"><span class="site-kecil">Cibitung</span></a>
                <a href="sentul" title="OMNI Cibitung"><span class="site-kecil">Sentul</span></a>
                <a href="tangerang" title="OMNI Cibitung"><span class="site-kecil">Tangerang</span></a>



            </div>
            <div id="ext-grid_wrapper" class="dataTables_wrapper no-footer">
                <div class="dataTables_length" id="ext-grid_length"><label>Menampilkan <select name="ext-grid_length"
                            aria-controls="ext-grid" class="">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select> data</label></div>
                <div id="ext-grid_filter" class="dataTables_filter" style="display: none;"><label>Cari Berdasarkan Kode
                        / Nama :<input type="search" class="" placeholder="" aria-controls="ext-grid"></label>
                </div>
                <div id="ext-grid_processing" class="dataTables_processing" style="display: none;">Harap Sabar Dalam
                    Proses...</div>
                <table id="ext-grid" class="display dataTable no-footer" role="grid"
                    aria-describedby="ext-grid_info" style="width: 100%;" width="100%" cellspacing="0"
                    cellpadding="0">
                    <thead>
                        <tr class="table_kepala" role="row">
                            <th class="sorting" tabindex="0" aria-controls="ext-grid" rowspan="1" colspan="1"
                                style="width: 169px;" aria-label="EXTENSION: activate to sort column ascending">
                                EXTENSION</th>
                            <th class="sorting" tabindex="0" aria-controls="ext-grid" rowspan="1" colspan="1"
                                style="width: 288px;" aria-label="NAMA: activate to sort column ascending">NAMA</th>
                            <th class="sorting_asc" tabindex="0" aria-controls="ext-grid" rowspan="1"
                                colspan="1" style="width: 196px;" aria-sort="ascending"
                                aria-label="DEPARTEMEN: activate to sort column descending">DEPARTEMEN</th>
                        </tr>
                    </thead>

                    <thead>
                        <tr>
                            <td><input type="text" data-column="0" class="search-input-ext"
                                    placeholder="cari berdasarkan no ext"></td>
                            <td><input type="text" data-column="1" class="search-input-text"
                                    placeholder="cari berdasarkan nama"></td>
                            <td> <select name="combo_cari" data-column="2" class="combo">
                                    <option value="" selected="">All Departemen</option>
                                    <option>ASURANSI</option>
                                    <option>BILLING</option>
                                    <option>CASEMIX</option>
                                    <option>DFS</option>
                                    <option>FARMASI</option>
                                    <option>FINANCE/ACCOUNTING</option>
                                    <option>HRD</option>
                                    <option>IGD</option>
                                    <option>IT</option>
                                    <option>KEPERAWATAN</option>
                                    <option>LABORATORIUM</option>
                                    <option>LAIN-LAIN</option>
                                    <option>LEGAL</option>
                                    <option>LOGISTIK</option>
                                    <option>MAINTENANCE/GA</option>
                                    <option>MARKETING</option>
                                    <option>MCU</option>
                                    <option>MEDIC</option>
                                    <option>MEDICAL RECORD</option>
                                    <option>PDS</option>
                                    <option>POLIKLINIK</option>
                                    <option>PURCHASING</option>
                                    <option>QRM</option>
                                    <option>RADIOLOGI</option>
                                </select>
                            </td>


                        </tr>
                    </thead>

                    <tbody>
                        <tr role="row" class="odd">
                            <td>1103</td>
                            <td>as admedika </td>
                            <td class="sorting_1">ASURANSI</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>1230</td>
                            <td>asuransi ward lt-5</td>
                            <td class="sorting_1">ASURANSI</td>
                        </tr>
                        <tr role="row" class="odd">
                            <td>1210</td>
                            <td>billing lANTAI 3</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>8100</td>
                            <td>kasir poli harmoni</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                        <tr role="row" class="odd">
                            <td>1298</td>
                            <td>billing LANTAI 5</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>8010</td>
                            <td>KASIR</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                        <tr role="row" class="odd">
                            <td>1119</td>
                            <td>as kasir asuransi</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>1218</td>
                            <td>drive thru kasir</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                        <tr role="row" class="odd">
                            <td>8116</td>
                            <td>kasir poli harmonI</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>1215</td>
                            <td>biling kasir drive thru</td>
                            <td class="sorting_1">BILLING</td>
                        </tr>
                    </tbody>
                </table>
                <div class="dataTables_info" id="ext-grid_info" role="status" aria-live="polite">Menampilkan 1 s/d
                    10 dari 361 data</div>
                <div class="dataTables_paginate paging_simple_numbers" id="ext-grid_paginate"><a
                        class="paginate_button previous disabled" aria-controls="ext-grid" data-dt-idx="0"
                        tabindex="0" id="ext-grid_previous">Previous</a><span><a class="paginate_button current"
                            aria-controls="ext-grid" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button "
                            aria-controls="ext-grid" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button "
                            aria-controls="ext-grid" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button "
                            aria-controls="ext-grid" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button "
                            aria-controls="ext-grid" data-dt-idx="5" tabindex="0">5</a><span>…</span><a
                            class="paginate_button " aria-controls="ext-grid" data-dt-idx="6"
                            tabindex="0">37</a></span><a class="paginate_button next" aria-controls="ext-grid"
                        data-dt-idx="7" tabindex="0" id="ext-grid_next">Next</a></div>
            </div>
        </div>
    </fieldset>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
        integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"
        integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS" crossorigin="anonymous">
    </script>



</body>
